<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Se registra el seeder que se va a utilizar
        $this->call(MessagesTableSeeder::class);
        $this->call(UserTableSeeder::class);
    }
}
