@extends('layout')

@section('contenido')
    <h1>Contactos</h1>
    <h2>Escribeme</h2>
    @if (session()->has('info'))
        {{ session('info') }}
    @else
    <form action="{{ route('mensajes.store') }}" method="POST">
        @include('messages.form',[
            'message' => new App\Message,
            'showFields' => auth()->guest()
        ])
    </form>
    @endif
@endsection