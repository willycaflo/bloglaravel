@extends('layout')

@section('contenido')
    <h1>Todos los mensajes</h1>

    <table class="table">
        <head>
            <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Email</th>
                <th>Mensaje</th>
                <th>Notas</th>
                <th>Etiquetas</th>
                <th>Acciones</th>
            </tr>
        </head>
        <tbody>
            @foreach ($messages as $message)
                <tr>
                    <td>{{ $message->id }}</td>
                    <td>{{ $message->present()->userName()  }}</td>
                    <td>{{ $message->present()->userEmail() }}</td>
                    <td>{{ $message->present()->link() }}</td>
                    <td>{{ $message->present()->notes() }}</td>
                    <td>{{ $message->present()->tags() }}</td>
                    <td>
                        <a href="{{ route("mensajes.edit",$message->id) }}" class="btn btn-info btn-xs">Editar</a>
                        <form style="display:inline;" action="{{ route("mensajes.destroy",$message->id) }}" method="POST">
                            {!! csrf_field() !!}
                            {!! method_field('DELETE') !!}
                            <button type="submit" class="btn btn-danger btn-xs">Eliminar</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            {{-- $messages->appends(request()->query())->links('pagination.custom') --}}
            {{ $messages->fragment('hash')->appends(request()->query())->links('pagination::default') }}
        </tbody>
    </table>
@endsection