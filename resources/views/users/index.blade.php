@extends('layout')

@section('contenido')
    <h1>Usuarios</h1>    
    <a href="{{ route('usuarios.create') }}" class="btn btn-primary pull-right">Crear nuevo usuario</a>
    <table class="table">
            <head>
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Email</th>
                    <th>Role</th>
                    <th>Notas</th>
                    <th>Etiquetas</th>
                    <th>Acciones</th>
                </tr>
            </head>
            <tbody>
                @foreach ($users as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->present()->link() }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->present()->roles() }}</td>
                        <td>{{ $user->present()->notes() }}</td>
                        <td>{{ $user->present()->tags() }}</td>
                        <td>
                            <a href="{{ route("usuarios.edit",$user->id) }}" class="btn btn-info btn-xs">Editar</a>
                            <form style="display:inline;" action="{{ route("usuarios.destroy",$user->id) }}" method="POST">
                                {!! csrf_field() !!}
                                {!! method_field('DELETE') !!}
                                <button type="submit" class="btn btn-danger btn-xs">Eliminar</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
@endsection
