<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="/css/app.css"/>
    <title>Mi sitio</title>
</head>
<body>
    <header>
        <!--h1>{{-- request()->url() --}}</h1-->

        <?php function activeMenu($url){
            return  request()->is($url) ? 'active' : '';
        }?>

        <nav class="navbar navbar-default" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Mi sitio</a>
                </div>
    
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav">
                        <li class="{{ activeMenu('/') }}">
                            <a href="{{ route('home') }}">Inicio</a>
                        </li>
                        <li class="{{ activeMenu('saludos/*') }}">
                            <a href="{{ route('saludos','Guillermo') }}">Saludos</a>
                        </li>
                        <li class="{{ activeMenu('mensajes/create') }}">
                            <a href="{{ route('mensajes.create') }}">Contactos</a>
                        </li>
                        @if(Auth()->check())
                        <li class="{{ activeMenu('mensajes*') }}">
                            <a href="{{ route('mensajes.index') }}">Mensajes</a>
                        </li>
                            @if(Auth()->user()->hasRoles(['admin']))
                            <li class="{{ activeMenu('mensajes*') }}">
                                <a href="{{ route('usuarios.index') }}">Usuarios</a>
                            </li>
                            @endif
                        @endif
                    </ul>
                    
                    <ul class="nav navbar-nav navbar-right">
                        @if(Auth()->guest())
                        <li class="{{ activeMenu('login') }}">
                            <a href="{{ route('login') }}">Login</a>
                        </li>
                        @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ Auth()->user()->name }} <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="/usuarios/{{ auth()->id() }}/edit">Mi cuenta</a>
                                </li>
                                <li class="{{ activeMenu('logout') }}">
                                    <a href="{{ route('logout') }}" 
                                        onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        Cerrrar sesión
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                        @endif
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div>
        </nav>

    </header>
    
    <div class="container">
        @yield('contenido')
        <br>
        <footer>Copyright &copy; {{ date('Y') }}</footer>
    </div>
    <script src="{{asset('js/jQuery/jquery-1.9.1.min.js')}}"></script>
    <script src="{{asset('js/bootstrap/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/script.js')}}"></script>
</body>
</html>