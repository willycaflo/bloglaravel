<?php

//use Illuminate\Support\Facades\App;
use App\User;

#use Symfony\Component\Routing\Route;
//use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('job',function(){
    dispatch(new App\Jobs\SendEmail);
    return "Listo!";
});

//Mostrar en pantalla las consultas SQL
DB::listen(function($query){
    //echo "<pre>{$query->sql}</pre>";
    //echo "<pre>{$query->time}</pre>";
});

Route::get('test',function(){
    $user = new User;
    $user->name = "Prueba";
    $user->email = "prueba@prueba.com";
    $user->password = bcrypt("Ejemplo123");
    $user->save();
    return $user;
});

Route::get('roles',function(){
    return \App\Role::with('user')->get();
});

//Route::get('/')->name('home')->uses('PagesController@home')->middleware('example');
Route::get('/')->name('home')->uses('PagesController@home');
Route::get('/contactame')->name('contactos')->uses('PagesController@contact');
Route::post('contacto','PagesController@mensajes');
Route::get('/saludos/{nombre?}')->name('saludos')->uses('PagesController@saludo')->where('nombre',"[A-Za-z]+");

//Se cambia todas las rutas del controlados a una sola linea
Route::resource('mensajes','MessagesController');
Route::resource('usuarios','UsersController');

/*Route::get('login','Auth\LoginController@showLoginForm');
Route::post('login','Auth\LoginController@login');*/

//Retornando vistas

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

/*Route::get('/mensajes')->name('messages.index')->uses('MessagesController@index');
Route::get('/mensajes/create')->name('messages.create')->uses('MessagesController@create');
Route::post('/mensajes')->name('messages.store')->uses('MessagesController@store');
Route::get('/mensajes/{id}')->name('messages.show')->uses('MessagesController@show');
Route::get('/mensajes/{id}/edit')->name('messages.edit')->uses('MessagesController@edit');
Route::put('/mensajes/{id}')->name('messages.update')->uses('MessagesController@update');
Route::delete('/mensajes/{id}')->name('messages.destroy')->uses('MessagesController@destroy');*/

//Pasar parametros por url
/*Route::get('/saludos/{nombre?}', function ($nombre = "Invitado") {
    //return "Saludos $nombre";
    //return view('saludo',['nombre' => $nombre]);
    //return view('saludo')->with(['nombre' => $nombre]);
    $html ="<h2>Contenido html</h2>";
    $script = "<script>alert('Problem XSS - Cross Site scripting!!!')</script>";

    $consolas = [
        'Play Station 4',
        'Xbox One',
        'Wii U',
        'Nintendo Switch'
    ];

    return view('saludo',compact('nombre','html','script','consolas'));
})->name('saludos');*/



//Parametros opcionales
/*Route::get('/saludos2/{nombre?}', function ($nombre = "Invitado") {
    return "Saludos $nombre";
});*/

//Validar un parametro
/*Route::get('/saludos3/{nombre?}', function ($nombre = "Invitado") {
    return "Saludos $nombre";
})->where('nombre',"[A-Za-z]+");*/


//Rutas con nombre
/*Route::get('/contacto', function () {
    //return "Esta es la seccion de contactos XD";
    return view('contactos');
})->name('contactos');*/

