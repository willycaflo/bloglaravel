<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //Metoso para validar que el susuario sea admin y pueda editar todo
    public function before($user, $ability){
        if($user->isAdmin()){
            return true;
        }
    }

    //Metodo para validar que el usuario autenticado y el usuario a editar sean el mismo y tenga acceso al metodo edit
    public function edit(User $authUser, User $user){
        return $authUser->id === $user->id;
    }
    //Metodo para revisar que el usuario autenticado y el usuario a editar sean el mismo y tenga acceso al metodo update
    public function update(User $authUser, User $user){
        return $authUser->id === $user->id;
    }

    //Metodo para revisar que el usuario autenticado y el usuario a editar sean el mismo y tenga acceso al metodo update
    public function destroy(User $authUser, User $user){
        return $authUser->id === $user->id;
    }
}
