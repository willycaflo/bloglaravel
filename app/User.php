<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Presenters\UserPresenter;
use Illuminate\Database\Eloquent\Model;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    //La nomenclatura para el nombre de estos metodos es set, para indicar que se va a modificar el valor antes de guardarlo
    //El segundo vaor el el nombre del campo, en esta caso password y por ultimo la palabra attribute
    public function setPasswordAttribute($password) {
        $this->attributes['password'] = bcrypt($password);
    }    

    //Metodo para establecer la realacion con la tabla roles
    public function roles() {
        return $this->belongsToMany(Role::class,'assigned_roles');
    }

    //Metodo para verificar que roles tiene el usuario
    public function hasRoles(array $roles) {
        return $this->roles->pluck('name')->intersect($roles)->count();
        /*foreach($roles as $role){
            foreach($this->roles as $userRole){
                if($userRole->name === $role){
                    return true;
                }
            }
        }
        return false;*/
    }

    //Metodo para saber si el usuario es admin
    public function isAdmin(){
        return $this->hasRoles(['admin']);
    }

    //Metodo para establecer la relacion con la tabla messages
    public function messages(){
        return $this->hasMany(Message::class);
    }

    //Agregar notas al usuario
    public function note(){
        return $this->morphOne(Note::class,'notable');
    }

    public function tags() {
        return $this->morphToMany(Tag::class, 'taggable')->withTimestamps();
    }

    public function present(){
        return new UserPresenter($this);
    }
}
