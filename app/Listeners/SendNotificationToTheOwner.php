<?php

namespace App\Listeners;

use Mail;
use App\Events\MessageWasReceived;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendNotificationToTheOwner implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MessageWasReceived  $event
     * @return void
     */
    public function handle(MessageWasReceived $event)
    {
        $message = $event->message;
        //El metodo send del obtejo Mail recive 3 parametros
        //1.- La vista
        //2.- Parametros para la vista
        //3.- Una funcion anonima con el parametro message
        Mail::send('emails.contact',['msg' => $message],function($msg) use ($message){
            $msg->from($message->email,$message->name)
                ->to('guillermo.cabrera@skyangel.com.mx','Guillermo')
                ->subject('Tu mensaje fue recibido');
        });
    }
}
