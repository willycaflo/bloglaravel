<?php

namespace App\Listeners;

use Mail;
use App\Events\MessageWasReceived;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendAutoresponder implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MessageWasReceived  $event
     * @return void
     */
    public function handle(MessageWasReceived $event)
    {
        $message = $event->message;
        if(auth()->check()){
            $message->email = auth()->user()->email;
        }
        //El metodo send del obtejo Mail recive 3 parametros
        //1.- La vista
        //2.- Parametros para la vista
        //3.- Una funcion anonima con el parametro message
        Mail::send('emails.contact',['msg' => $message],function($msg) use ($message){
            $msg->to($message->email,$message->name)->subject('Tu mensaje fue recibido');
        });
    }
}
