<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use App\Http\Requests\CreateMessageRequest;

class PagesController extends Controller
{
    public function __construct(){
        $this->middleware('example',['only' => ['home']]);
    }
    public function home(){
        return view('messages.home');
    }

    public function contact(){
        return view('contactos');
    }

    public function saludo($nombre = "Invitado"){
        $html ="<h2>Contenido html</h2>";
        $script = "<script>alert('Problem XSS - Cross Site scripting!!!')</script>";
        $consolas = [
            'Play Station 4',
            'Xbox One',
            'Wii U',
            'Nintendo Switch'
        ];
        return view('saludo',compact('nombre','html','script','consolas'));
    }

    public function mensajes(CreateMessageRequest $request){
        //return $request->all();
        /*return response($request->all(),200)
                        ->header('X-TOKEN','secret')
                        ->cookie('X-COOKIE','cookie');*/
        $data = $request->all();
        /*return redirect()
                ->route('contactos')
                ->with('info','Tu mensaje ha sido enviado correctamente.');*/
        //Regresar a la ruta anterior
        return back()->with('info','Tu mensaje ha sido enviado correctamente.');
    }
}
