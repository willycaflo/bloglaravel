<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use Illuminate\Http\Request;
use App\Http\Requests\RegisterUserRequest;
use App\Http\Requests\UpadateUserRequest;

class UsersController extends Controller
{
    public function __construct()
    {
        /*$this->middleware(
            [
                'auth',
                'roles:admin'
            ]
        );*/
        $this->middleware('auth',['except' => ['show']]);
        $this->middleware('roles:admin',['except' => ['edit','update','show']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$users = \App\User::all();
        $users = User::with(['roles','note','tags'])->get();
        return view('users.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('display_name','id');
        return view('users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\RegisterUserRequest
     * @return \Illuminate\Http\Response
     */
    public function store(RegisterUserRequest $request)
    {
        //Se crea el nuevo usuario
        $user = User::create($request->all());
        //Se le asignan los roles
        $user->roles()->attach($request->roles);
        //Se redirige a la ventana index
        return redirect()->route('usuarios.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        return view('users.show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        //Se verifica si esta autrizado para editar
        $this->authorize('edit',$user);

        $roles = Role::pluck('display_name','id');

        return view('users.edit',compact('user','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpadateUserRequest $request, $id)
    {
        $user = User::findOrFail($id);
        //Se verifica si esta autorizado para el metodo update
        $this->authorize('update',$user);

        //Que solo actualice nombre y correo
        $user->update($request->only('name','email'));
        //Asignacion de los roles recibidos por formulario
        #$user->roles()->attach($request->roles);
        //Para evitar que cada que se realice el atach los roles se supliquen se usa el metodo sync
        $user->roles()->sync($request->roles);

        return back()->with('info','Usuario actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        //Se verifica si esta autorizado para el metodo destroy
        $this->authorize('destroy',$user);
        
        $user->delete();
        return back();
    }
}
