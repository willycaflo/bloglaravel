<?php

namespace App\Http\Controllers;

use DB;
use Mail;
use Carbon\Carbon;
use App\Message;
use Illuminate\Http\Request;
use App\Http\Requests\CreateMessageRequest;
use App\Events\MessageWasReceived;
use Illuminate\Support\Facades\Cache;
use App\Repositories\Messages;
//use App\Repositories\CacheMessages;
use App\Repositories\MessagesInterface;

class MessagesController extends Controller
{
    protected $messages;

    //public function __construct(CacheMessages $messages){
        public function __construct(Messages $messages){
        $this->messages = $messages;
        $this->middleware('auth',['except' => ['create','store']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$key = "messages.page.".request("page",1);
        //Taer todos los registros de la tabla
        //Query Builder
        #$messages = DB::table('messages')->get();
        //Eloquent
        //$messages = Message::all();
        //$messages = Cache::remember($key,5,function(){
        /*$messages = Cache::tags('messages')->rememberForever($key,function(){
            //Para paginar los resultados se cambia el metodo get por paginate
            return Message::with(['user','note','tags'])
                        ->orderBy('created_at',request('sorted','DESC'))
                        ->paginate(10);
        });*/
        /*if(Cache::has($key)){
            $messages = Cache::get($key);    
        }else{
            //Para paginar los resultados se cambia el metodo get por paginate
            $messages = Message::with(['user','note','tags'])
                        ->orderBy('created_at',request('sorted','ASC'))
                        ->paginate(10);
            Cache::put($key,$messages,5);
        }*/
        $messages = $this->messages->getPaginated();
        return view('messages.index',compact('messages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('messages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateMessageRequest $request)
    {
        //Guardar mensaje
        //Query builder
        /*DB::table('messages')->insert([
            "nombre" => $request->input('nombre'),
            "email" => $request->input('email'),
            "mensaje" => $request->input('mensaje'),
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
        ]);*/
        //Eloquent
        //--Forma 1
        /*$message = new Message();
        $message->nombre = $request->input('nombre');
        $message->email = $request->input('email');
        $message->mensaje = $request->input('mensaje');
        $message->save();*/
        //--Forma 2
        /*Message::create([
            "nombre" => $request->input('nombre'),
            "email" => $request->input('email'),
            "mensaje" => $request->input('mensaje'),
        ]);*/
        //--Forma 3
        #$message = Message::create($request->all());
        
        /*if(auth()->check()){
            auth()->user()->messages()->save($message);
        }*/

        //Limpiar la cache
        #Cache::tags('messages')->flush();
        
        //El metodo send del obtejo Mail recive 3 parametros
        //1.- La vista
        //2.- Parametros para la vista
        //3.- Una funcion anonima con el parametro message
        /*Mail::send('emails.contact',['msg' => $message],function($msg) use ($message){
            $msg->to($message->email,$message->name)->subject('Tu mensaje fue recibido');
        });*/
        $message = $this->messages->store($request);
        //Ahora el envio del correo se realiza mediante un evento
        event(new MessageWasReceived($message));

        //Esta forma sirve cuando savemos que siempre tenemos un usuario autenticado
        //auth()->user()->messages()->create($request->all());
        
        return redirect()->route('mensajes.create')->with('info','Hemos recibido tu mensaje');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /*$message = Cache::tags('messages')->rememberForever("message.{$id}",function() use ($id){
            //Eloquent 
            return Message::findOrFail($id);
        });*/
        //Obtener un mensaje
        //Query Builder
        #$message = DB::table("messages")->where("id",$id)->first();
        //Eloquent 
        //$message = Message::findOrFail($id);
        $message = $this->messages->findById($id);
        return view('messages.show',compact('message'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Obtener mensaje
        //Query Builder
        #$message = DB::table("messages")->where("id",$id)->first();
        /*$message = Cache::tags('messages')->rememberForever("message.{$id}",function() use ($id){
            //Eloquent 
            return Message::findOrFail($id);
        });*/
        //Eloquent
        //$message = Message::findOrFail($id);
        $message = $this->messages->findById($id);
        return view("messages.edit",compact("message"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Actualizar mensaje
        //Query Builder
        /*DB::table('messages')->where('id',$id)->update([
            "nombre" => $request->input('nombre'),
            "email" => $request->input('email'),
            "mensaje" => $request->input('mensaje'),
            "updated_at" => Carbon::now()
        ]);*/
        //Eloquent
        #$message = Message::findOrFail($id);
        #$message->update($request->all());
        #Cache::tags('messages')->flush();
        $this->messages->update($request,$id);
        return redirect()->route('mensajes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Eliminar Mensaje
        //Query Builder
        #DB::table('messages')->where('id',$id)->delete();
        //Eloquent
        /*$message = Message::findOrFail($id);
        $message->delete();
        Cache::tags('messages')->flush();*/
        $this->messages->destroy($id);
        //Redirecconar
        return redirect()->route('mensajes.index');
    }
}
